# Calcul différentiel et EDO - Etudiants - MODIA

Pour récupérer les sources, il faut cloner ce dépot git. Faire la commande suivante dans un terminal :

```bash
git clone https://gitlab.irit.fr/toc/mathn7/calcul-diff-and-edo-modia/etudiants.git
```

Vous pouvez ensuite renommer le répertoire parent. Une autre possibilité est de le faire directement par le clone :

```bash
git clone repo-name folder-name
```
